import platform

# platform implementation
impl = platform.python_implementation()
ver = platform.version() # return version
mach = platform.machine() # return machine
sys = platform.system() # return system

print('Python impl:    ' + impl)
print('Python version: ' + ver)
print('Python machine: ' + mach)
print('Python system:  ' + sys)
print('Python version: ' + platform.python_version())
