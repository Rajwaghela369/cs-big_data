# Assignment A: Setup Python

## **Challenges**

### 1. Termainal:

>(base) rajwaghela@Rajs-MBP-2 ~ % cd Downloads/Projects/cs-big_data

>(base) rajwaghela@Rajs-MBP-2 cs-big_data % pwd

    /Users/rajwaghela/Downloads/Projects/cs-big_data

>(base) rajwaghela@Rajs-MBP-2 cs-big_data % ls -la 

    total 0
    drwxr-xr-x@  6 rajwaghela  staff  192 Oct 23 13:58 .
    drwxr-xr-x   8 rajwaghela  staff  256 Oct 23 13:30 ..
    drwxr-xr-x  12 rajwaghela  staff  384 Oct 23 14:02 .git
    drwxr-xr-x   9 rajwaghela  staff  288 Oct 23 14:08 .idea
    drwxr-xr-x   4 rajwaghela  staff  128 Oct 23 14:08 Assignment-A
    -rw-r--r--   1 rajwaghela  staff    0 Oct 23 13:31 README.md

>(base) rajwaghela@Rajs-MBP-2 cs-big_data % whoami 
        
        rajwaghela

>(base) rajwaghela@Rajs-MBP-2 cs-big_data % cat .profile

> (base) rajwaghela@Rajs-MBP-2 cs-big_data % cat .bashsrc


> (base) rajwaghela@Rajs-MBP-2 cs-big_data % echo $path
    
    /Users/rajwaghela/anaconda3/bin /Users/rajwaghela/anaconda3/condabin /usr/local/bin /System/Cryptexes/App/usr/bin /usr/bin /bin /usr/sbin /sbin /var/run/com.apple.security.cryptexd/codex.system/bootstrap/usr/local/bin /var/run/com.apple.security.cryptexd/codex.system/bootstrap/usr/bin /var/run/com.apple.security.cryptexd/codex.system/bootstrap/usr/appleinternal/bin /opt/X11/bin

### 2. Python3:
> (base) rajwaghela@Rajs-MBP-2 cs-big_data % python --version 
>
    Python 3.10.9

### 3. PIP:
> (base) rajwaghela@Rajs-MBP-2 cs-big_data % pip --version   
>
    pip 22.3.1 from /Users/rajwaghela/anaconda3/lib/python3.10/site-packages/pip (python 3.10)

### 4. Test Python:

>(base) rajwaghela@Rajs-MBP-2 cs-big_data % python 
>
    Python 3.10.9 (main, Mar  1 2023, 12:33:47) [Clang 14.0.6 ] on darwin
    Type "help", "copyright", "credits" or "license" for more information.

    >>> print('Hello World')
    Hello World

    >>> help('modules')
    AppKit              cloudpickle         keras               retrying
    Cocoa               clyent              keras_preprocessing rich
    CoreFoundation      cmath               keyring             rlcompleter
    CoreServices        cmd                 keyword             rope
    Cython              code                kiwisolver          rsa

    >>> 2+5+6
    13
    >>> x = 2+3*8
    >>> x
    26
<BR></BR>
### run print_sys.py: 

    import platform
    
    # platform implementation
    impl = platform.python_implementation()
    ver = platform.version() # return version
    mach = platform.machine() # return machine
    sys = platform.system() # return system
    
    print('Python impl:    ' + impl)
    print('Python version: ' + ver)
    print('Python machine: ' + mach)
    print('Python system:  ' + sys)
    print('Python version: ' + platform.python_version())



    output: 
    /usr/bin/python3 /Users/rajwaghela/Downloads/Projects/cs-big_data/Assignment-A/print_sys.py 
    Python impl:    CPython
    Python version: Darwin Kernel Version 23.0.0: Fri Sep 15 14:41:43 PDT 2023; root:xnu-10002.1.13~1/RELEASE_ARM64_T6000
    Python machine: arm64
    Python system:  Darwin
    Python version: 3.9.6
    
    Process finished with exit code 0
