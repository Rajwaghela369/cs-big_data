# Assignments B: Explore Python
##  Challenges:

### 1. Indexing Fruits:
    >>> fruits = ['apple', 'pear', 'orange', 'banana', 'apple']
    >>> fruitbag = {'apple', 'pear', 'orange', 'banana', 'apple'}
    >>> fruitbox = ('apple', 'pear', 'orange', 'banana', 'apple')
    
    >>> print(f'Third Fruit is: {fruits[2]}')
    Third Fruit is: orange
    
    >>> print(f'Second and Third Fruit is: {fruits[1:3]}')
    Second and Third Fruit is: ['pear', 'orange']
    

    >>> print(f'The Last Fruit is: {fruits[-1]}')
    The Last Fruit is: apple
    
    >>> print(f'The Second Last Fruit is: {fruits[-2]}')
    The Second Last Fruit is: banana
    
    >>> print(f'The Last Three  Fruit are: {fruits[-3:]}')
    The Last Three  Fruit are: ['orange', 'banana', 'apple']

### 2. Packaging Fruits:

        >>> print(fruits)
        ['apple', 'pear', 'orange', 'banana', 'apple']
        
        >>> print(fruitabg)
        Traceback (most recent call last):
          File "<stdin>", line 1, in <module>
        NameError: name 'fruitabg' is not defined. Did you mean: 'fruitbag'?
        
        >>> print(fruits)
        ['apple', 'pear', 'orange', 'banana', 'apple']

        >>> print(fruitbag)
        {'banana', 'apple', 'pear', 'orange'}
        
        >>> print(fruitbox)
        ('apple', 'pear', 'orange', 'banana', 'apple')
        
        >>> print(fruits[1])
        pear
        
        >>> print(fruitbag[1])
        Traceback (most recent call last):
          File "<stdin>", line 1, in <module>
        TypeError: 'set' object is not subscriptable
        
        >>> print(fruitbox[1])
        pear
        


