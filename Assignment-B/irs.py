import pandas as pd
import numpy as np
data = pd.read_csv('20zpallagi.csv')
zips = [93636,94040,94304,94027,50860,10023]

# Define a function to calculate the mean income for a given ZIP code
def mean(zip_code):
    # Filter the data for the specified ZIP code and calculate the mean
    mean_zip = np.mean(data[data['zipcode'] == zip_code]['A02650'])
    country = data[data['zipcode'] == zip_code]['STATE'].iloc[1]
    return mean_zip, country

# Define a function to calculate the median income for a given ZIP code
def median(zip_code):
    # Filter the data for the specified ZIP code and calculate the median
    median_zip = np.median(data[data['zipcode'] == zip_code]['A02650'])
    return median_zip

# Example usage
for zip_code in zips:
    mean_in, name = mean(zip_code)
    median_in = median(zip_code)
    print(f'mean_income in, {name}  is: {mean_in} - median_income is: {median_in}')