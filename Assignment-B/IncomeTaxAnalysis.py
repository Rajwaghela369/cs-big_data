# Define a data structure to store tax data for a ZIP code
zip_data = {
    "93636": {
        "income_brackets": [
            {"range": (1, 25000), "count": 1800},
            {"range": (25000, 50000), "count": 1380},
            {"range": (50000, 75000), "count": 980},
            {"range": (75000, 100000), "count": 830},
            {"range": (100000, 200000), "count": 1660},
            {"range": (200000, 10000000), "count": 550}
        ]
    },
    # Add data for other ZIP codes here if needed
}

# Define a function to calculate the mean income for a ZIP code
def mean_income(zip_code):
    total_income = 0
    total_returns = 0
    for bracket in zip_data.get(zip_code, {}).get("income_brackets", []):
        total_income += (bracket["range"][0] + bracket["range"][1]) / 2 * bracket["count"]
        total_returns += bracket["count"]
    return int(total_income / total_returns)

# Define a function to calculate the median income for a ZIP code
def median_income(zip_code):
    income_list = []
    for bracket in zip_data.get(zip_code, {}).get("income_brackets", []):
        lower, upper = bracket["range"]
        income_list.extend([upper] * bracket["count"])
    income_list.sort()
    if len(income_list) % 2 == 0:
        mid = len(income_list) // 2
        return int((income_list[mid - 1] + income_list[mid]) / 2)
    else:
        mid = len(income_list) // 2
        return income_list[mid]

# Define a function to return the total number of returns for a ZIP code
def number_of_returns(zip_code):
    total_returns = 0
    for bracket in zip_data.get(zip_code, {}).get("income_brackets", []):
        total_returns += bracket["count"]
    return total_returns


# Output the results
def print_analysis(zip_code):
    _county = 'Madera, CA'
    print(
        f'mean_income in {_county:26} is: {mean_income(zip_code):10,} - ' +
        f'median_income is: {median_income(zip_code):8,}' +
        f' - number of returns: {number_of_returns(zip_code):6,}'
    )

# Demonstrate the functions
if __name__ == '__main__':
    zip_code = "93636"
    print_analysis(zip_code)